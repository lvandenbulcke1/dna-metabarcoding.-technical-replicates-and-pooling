# Abstract
DNA-based monitoring methods are potentially faster and cheaper compared to traditional morphological benthic identification. DNA metabarcoding involves various methodological choices which can introduce bias leading to a different outcome in biodiversity patterns. Therefore, it is important to harmonize DNA metabarcoding protocols to allow comparison across studies and this requires a good understanding of  the impact of methodological choices on diversity estimates. This study investigated the impact of DNA and PCR replicates on the detection of macrobenthos species in locations with high, medium and low diversity locations. Our results show that three DNA replicates were needed in locations with a high and medium diversity to detect at least 80% of the species found in the six DNA replicates, while four replicates were needed in the low diverse location. In contrast to general belief, larger body size or higher abundance of the species in a sample did not increase its detection prevalence among DNA replicates. However, rare species were less consistently detected across all DNA replicates of the most diverse location with high diversity compared to less diverse locations with less diversity. Our results further show that pooling of DNA replicates did not significantly alter diversity patterns, although a small number of rare species was lost. Finally, our results confirm high variation in species detection between PCR replicates, especially for the detection of rare species. These results contributeallow to create reliable, time and cost efficient metabarcoding protocols yielding reliable results for the characterization of macrobenthos.

# Different R scripts

- STEP 2 _ DADA2 pipeline
- STEP 3 _ Alpha diversity, numer of reads and ASVs (non rarified data): DNA replicates, pooled DNA extractions and PCR replicates
- STEP 4 _ Alpha diversity, ASVs (rarified data): DNA replicates, pooled DNA extractions and PCR replicates
- STEP 5 a _ Alpha diversity, Species level: DNA replicates
- STEP 5 b _ Alpha diversity, Species level: Pooled DNA extractions
- STEP 5 c _ Alpha diversity, Species level: PCR replicates
- STEP 6 a _ Beta diversity, Permanova: DNA replicates
- STEP 6 ab _ Beta diveristy, Permanova: Pooled DNA extractions

