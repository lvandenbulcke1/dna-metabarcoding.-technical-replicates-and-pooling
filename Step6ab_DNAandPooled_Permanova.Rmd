---
title: "Step6ab_DNAandPooled_Permanova"
author: "Laure Van den Bulcke"
date: "27-5-2021"
output: html_document
---

-----
INPUT
-----

Beta diversity: only look at rarified data 
```{r Read data}
if(.Platform$OS.type == "unix"){home<-"/home/"} else{
  home<-"//192.168.236.131/"}

#1 read data
setwd(paste0(home,"/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl"))
table_rarified_DNA <- read.table("CountTable_rarified_DNA.txt",sep=",",header=T)

#2: remove RMG code
names_DNA<-as.character(table_rarified_DNA$Species) #store the names of the species in a vector
names_DNA2<-strsplit(names_DNA, "_")#split according to underscore
names_DNA2[is.na (names_DNA2)]<-"NA" #change missing values to "NA"
table_rarified_DNA$Species<-unlist(lapply(names_DNA2,'[[',1))#only keep the first part of all elements in the list; 

rownames(table_rarified_DNA) <- table_rarified_DNA$ASV

rm(names_DNA, names_DNA2)

#3: sort by species
taxo <- "Species"
merged_data_rarified_DNA<-aggregate(table_rarified_DNA[,1:78], by= list(as.factor(table_rarified_DNA[,taxo])),FUN=sum)

#Set the tax names as rownames
rownames(merged_data_rarified_DNA)<-merged_data_rarified_DNA$Group.1
merged_data_rarified_DNA$Group.1<-NULL

#remove NAs
merged_data_rarified_DNA <- merged_data_rarified_DNA[rownames(merged_data_rarified_DNA)!= "NA", ]

```


De nodige dataframes worden gemaakt uit de ingelezen data:
1) informatie over de soorten: rijen= stalen, kolommen= soortsnamen
2) extra informatie: rijen= stalen, kolommen= station, biologische & DNA replicaten

```{r Needed dataframes}
library(vegan)

data_soorten <- t(merged_data_rarified_DNA)
data_soorten <- data_soorten 
replicaten_name <- rownames(data_soorten)

data_replicaten<- as.data.frame(rownames(data_soorten))
colnames(data_replicaten)<-"V1"
data_replicaten <- data_replicaten %>% separate(V1,c(NA,"Station","Biol_replicate","DNA_repl",NA,NA),sep="_")
data_replicaten$Station <- as.factor(data_replicaten$Station)
data_replicaten$Biol_replicate <- as.factor(data_replicaten$Biol_replicate)
data_replicaten$DNA_repl <- as.factor(data_replicaten$DNA_repl)
data_replicaten$Station_biol <- paste(data_replicaten$Station,data_replicaten$Biol_replicate,sep="_")
data_replicaten$Station_repl <- paste(data_replicaten$Station,data_replicaten$DNA_repl,sep="_")
nr_pooled <- c()
for (i in 1:nrow(data_replicaten)){
  nr <- nchar(as.character(data_replicaten$DNA_repl[i]))
  nr_pooled <- append(nr_pooled, nr)
}
data_replicaten$Nr_pooled <- nr_pooled
data_replicaten$Biol_nr <- paste(data_replicaten$Biol_replicate, data_replicaten$Nr_pooled, sep="_")
```



```{R Subsets maken}
data_soorten_120 <- data_soorten[grep("120_B", rownames(data_soorten)),]
data_soorten_840 <- data_soorten[grep("840_C", rownames(data_soorten)),]
data_soorten_ZVL <- data_soorten[grep("ZVL_A", rownames(data_soorten)),]

data_replicaten_120 <- data_replicaten[data_replicaten$Station_biol=="120_B", ]
data_replicaten_840 <- data_replicaten[data_replicaten$Station_biol=="840_C", ]
data_replicaten_ZVL <- data_replicaten[data_replicaten$Station_biol=="ZVL_A", ]

data_soorten2 <- as.data.frame(data_soorten)
data_soorten2$bio <- data_replicaten$Station_biol
data_soorten_stations <- data_soorten2[data_soorten2$bio %in% c("120_B","840_C","ZVL_A"), ]
data_soorten_stations <- data_soorten_stations[,1:(ncol(data_soorten_stations)-1)]

data_replicaten_stations <- as.data.frame(rownames(data_soorten_stations))
colnames(data_replicaten_stations)<-"V1"
data_replicaten_stations <- data_replicaten_stations %>% separate(V1,c(NA,"Station",NA,"DNA_repl",NA,NA),sep="_")
data_replicaten_stations$Station <- as.factor(data_replicaten_stations$Station)
data_replicaten_stations$DNA_repl <- as.factor(data_replicaten_stations$DNA_repl)
nr_pooled <- c()
for (i in 1:nrow(data_replicaten_stations)){
  nr <- nchar(as.character(data_replicaten_stations$DNA_repl[i]))
  nr_pooled <- append(nr_pooled, nr)
}
data_replicaten_stations$Nr_pooled <- nr_pooled

diversity_column <- c()
for (i in 1:nrow(data_replicaten_stations)){
  station <- data_replicaten_stations$Station[i]
  if (station == "120") {diversity <- "HIGH"}
  if (station == "840") {diversity <- "MEDIUM"}
  if (station == "ZVL") {diversity <- "LOW"}
  diversity_column <- append(diversity_column, diversity)
}
data_replicaten_stations$diversity <- diversity_column
data_replicaten_stations$diversity <- as.factor(data_replicaten_stations$diversity)

rm(data_replicaten)
```



```{R Dissimilarity index}

dist_soorten_jaccard <- vegdist(data_soorten_stations, method = "jaccard",binary=TRUE)

#fourth root transformation
data_soorten_4thRoot <- data_soorten_stations^(1/4)
dist_soorten_4thRoot_jaccard <- vegdist(data_soorten_4thRoot, method = "jaccard",binary=TRUE)

# BRAY
range(data_soorten^0.25)

dist_soorten_bray <- vegdist(data_soorten_stations^0.25, method = "bray") 


```



Eerst alle stations samen
```{R nmds plot stations}

#alle stations samen
nmds_stations <- metaMDS(dist_soorten_jaccard)

data_replicaten_stations$Nr_pooled <- as.factor(data_replicaten_stations$Nr_pooled)
data_replicaten_stations$diversity <- factor(data_replicaten_stations$diversity, levels= c("LOW", "MEDIUM", "HIGH"))


p <- ordiplot(nmds_stations, type = "n", xlim=c(-0.75,0.6))
pchs <- c(0, 1, 2, 3)
cols <- c("blue", "green", "orange")
#points(nmds_stations, cex = 2,col = "black")
points(nmds_stations, cex = 1, pch = pchs[data_replicaten_stations$Nr_pooled], col = cols[data_replicaten_stations$diversity])
legend("bottomleft", pch = pchs, col = "black", legend = levels(as.factor(data_replicaten_stations$Nr_pooled)), title="Nr_pooled")
legend("topleft", pch = 15, col = cols, legend = levels(as.factor(data_replicaten_stations$diversity)), title="Diversity")
#orditorp(nmds_stations, display = "sites", scaling=scl,cex=0.5) #, priority = priSpp, scaling = scl, col = "forestgreen", pch = 2, cex = 1)

#bray
nmds_stations_bray <- metaMDS(dist_soorten_bray)
p <- ordiplot(nmds_stations_bray$points)

p <- ordiplot(nmds_stations_bray, type = "n")
pchs <- c(0, 1, 2, 3)
cols <- c("blue", "green", "orange")
#points(nmds_stations, cex = 2,col = "black")
points(nmds_stations_bray, cex = 2, pch = pchs[data_replicaten_stations$Nr_pooled], col = cols[data_replicaten_stations$diversity])
legend("bottomleft", pch = pchs, col = "black", legend = levels(as.factor(data_replicaten_stations$Nr_pooled)), title="Nr_pooled")
legend("topleft", pch = 15, col = cols, legend = levels(as.factor(data_replicaten_stations$diversity)), title="Diversity")

```




```{R assumptions permanova stations}

station_p <- data_replicaten_stations$Station
nr_pooled_p <- data_replicaten_stations$Nr_pooled
station_nr_pooled_p <- paste(data_replicaten_stations$Station, data_replicaten_stations$Nr_pooled)

pmv_test <- adonis2(dist_soorten_jaccard ~   station_p * nr_pooled_p, data=data_replicaten_stations, permutations=9999) 
pmv_test #sign voor station, niet voor nr_pooled

pmv_test2 <- adonis(dist_soorten_jaccard ~   station_p * nr_pooled_p, data=data_replicaten_stations, permutations=9999) 
pmv_test #geeft hetzelfde resultaat

repl_betadisper <- betadisper(dist_soorten_jaccard, station_p, type="centroid")
repl_permdisp <- permutest(repl_betadisper, permutations=9999)
repl_permdisp #  sign 
plot(repl_betadisper, hull=FALSE, ellipse=TRUE)


repl_betadisper <- betadisper(dist_soorten_jaccard, nr_pooled_p, type="centroid")
repl_permdisp <- permutest(repl_betadisper, permutations=9999)
repl_permdisp #  NOT sign 
plot(repl_betadisper, hull=FALSE, ellipse=TRUE)



#4th root transformation
#pmv_test <- adonis2(dist_soorten_4thRoot_jaccard ~   station_p * nr_pooled_p, data=data_replicaten_stations, permutations=9999) 
#pmv_test

data_soorten_4thRoot <- (data_soorten_stations)^(1/4)
dist_soorten_4thRoot_jaccard <- vegdist(data_soorten_4thRoot, method = "jaccard",binary=TRUE)

repl_betadisper <- betadisper(dist_soorten_4thRoot_jaccard, station_p, type="centroid")
repl_permdisp <- permutest(repl_betadisper, permutations=9999)
repl_permdisp #  sign 
plot(repl_betadisper, hull=FALSE, ellipse=TRUE)


#DOOR 1 STAAL: NOG STEEDS SIGNIFICANTE VERSCHILLEN IN DISPERSIE
#S62_120_B_624351_6_D
data_soorten_less <- data_soorten_stations[rownames(data_soorten_stations)!= "S62_120_B_624351_6_D",]
data_replicaten_less <- data_replicaten_stations[data_replicaten_stations$DNA_repl !="624351",]
dist_soorten_less <- vegdist(data_soorten_less, method = "jaccard",binary=TRUE)
station_p <- data_replicaten_less$Station
repl_betadisper <- betadisper(dist_soorten_less, station_p, type="centroid")
repl_permdisp <- permutest(repl_betadisper, permutations=9999)
repl_permdisp #  sign 
plot(repl_betadisper, hull=FALSE, ellipse=TRUE)


```

```{R, PERMANOVA Bray}

station_p <- data_replicaten_stations$Station
nr_pooled_p <- data_replicaten_stations$Nr_pooled
station_nr_pooled_p <- paste(data_replicaten_stations$Station, data_replicaten_stations$Nr_pooled)

pmv_test <- adonis2(dist_soorten_bray ~   station_p * nr_pooled_p, data=data_replicaten_stations, permutations=9999) 
pmv_test #sign voor station, niet voor nr_pooled


repl_betadisper <- betadisper(dist_soorten_bray, station_p, type="centroid")
repl_permdisp <- permutest(repl_betadisper, permutations=9999)
repl_permdisp #  NOT sign 
plot(repl_betadisper, hull=FALSE, ellipse=TRUE)


repl_betadisper <- betadisper(dist_soorten_bray, nr_pooled_p, type="centroid")
repl_permdisp <- permutest(repl_betadisper, permutations=9999)
repl_permdisp #  NOT sign 
plot(repl_betadisper, hull=FALSE, ellipse=TRUE)


```


Visueel bepalen of er onderscheid in groepen kan gemaakt worden via NMDS plots

```{R NMDS plot, echo=F}
#dissimilarity index = jaccard (prensence/absence)
dist_soorten_120 <- vegdist(data_soorten_120, method = "jaccard",binary=TRUE) #Bray-Curtis blijkbaar vrij gelijkend
dist_soorten_840 <- vegdist(data_soorten_840, method = "jaccard",binary=TRUE)
dist_soorten_ZVL <- vegdist(data_soorten_ZVL, method = "jaccard",binary=TRUE)


#NMDS
nmds_120 <- metaMDS(dist_soorten_120)
nmds_840 <- metaMDS(dist_soorten_840)
nmds_ZVL <- metaMDS(dist_soorten_ZVL)

nmds_stations <- metaMDS(dist_soorten_stations)

#kijken naar verschillende stations
p <- ordiplot(nmds_120, type = "n")
pchs <- c(0, 1, 2, 3)
points(nmds_120, cex = 2, pch = pchs[data_replicaten_120$Nr_pooled], col = "black")
#ordispider(nmds_120, groups = data_replicaten_120$Biol_nr, label = F)
legend("topleft", pch = pchs, col = "black", legend = levels(as.factor(data_replicaten_120$Nr_pooled)),title="Nr_pooled")
#orditorp(nmds_120,display="sites",col="black",air=1,cex=0.4)

p <- ordiplot(nmds_840, type = "n")
pchs <- c(0,1, 2, 3)
points(nmds_840, cex = 2, pch = pchs[data_replicaten_840$Nr_pooled], col = "black")
#ordispider(nmds_840, groups = data_replicaten_840$Biol_nr, label = F)
legend("topleft", pch = pchs, col = "black", legend = levels(as.factor(data_replicaten_840$Nr_pooled)),title="Nr_pooled")
#orditorp(nmds_840,display="sites",col="black",air=1,cex=0.4)

p <- ordiplot(nmds_ZVL, type = "n")
pchs <- c(0,1, 2, 3)
points(nmds_ZVL, cex = 2, pch = pchs[data_replicaten_ZVL$Nr_pooled], col = "black")
#ordispider(nmds_ZVL, groups = data_replicaten_ZVL$Biol_nr, label = F)
legend("topleft", pch = pchs, col = "black", legend = levels(as.factor(data_replicaten_ZVL$Nr_pooled)), title="Nr_pooled")
#orditorp(nmds_ZVL,display="sites",col="black",air=1,cex=0.4)





```


```{R, 120 zonder echinocardium}
data_soorten_120_echino <- data_soorten_120[, colnames(data_soorten_120) != "Ophiura ophiura | Liocarcinus marmoreus"]
dist_soorten_120_echino <- vegdist(data_soorten_120_echino, method = "jaccard",binary=TRUE) 
nmds_120_echino <- metaMDS(dist_soorten_120_echino)
data_replicaten_120_echino <- data_replicaten_120

p <- ordiplot(nmds_120_echino, type = "n")
cols <- c("red", "blue", "green")
pchs <- c(0,1, 2, 3)
points(nmds_120_echino, cex = 2, pch = pchs[data_replicaten_120_echino$Nr_pooled], col = cols[data_replicaten_120_echino$Biol_replicate])
legend("topright", pch = pchs, col = "black", legend = levels(as.factor(data_replicaten_120_echino$Nr_pooled)))
#orditorp(nmds_120_echino,display="sites",col="black",air=1,cex=0.4)

```




TOT HIER PAPER
```{R, permanova DNA extracts}
data_replicaten_120_1 <-data_replicaten_120[data_replicaten_120$Nr_pooled=="1", ] 
data_soorten_120_1 <- data_soorten_120[grep("1_D", rownames(data_soorten_120)),]
dist_soorten_120_1 <-  vegdist(data_soorten_120_1, method = "jaccard",binary=TRUE)

data_replicaten_840_1 <-data_replicaten_840[data_replicaten_840$Nr_pooled=="1", ] 
data_soorten_840_1 <- data_soorten_840[grep("1_D", rownames(data_soorten_840)),]
dist_soorten_840_1 <-  vegdist(data_soorten_840_1, method = "jaccard",binary=TRUE)

data_replicaten_ZVL_1 <-data_replicaten_ZVL[data_replicaten_ZVL$Nr_pooled=="1", ] 
data_soorten_ZVL_1 <- data_soorten_ZVL[grep("1_D", rownames(data_soorten_ZVL)),]
dist_soorten_ZVL_1 <-  vegdist(data_soorten_ZVL_1, method = "jaccard",binary=TRUE)


#station 120_1
biol_repl120_1_p <- data_replicaten_120_1$Biol_replicate
data_replicaten_120_1$biol_pooled <- paste(data_replicaten_120_1$Biol_replicate, data_replicaten_120_1$Nr_pooled, sep="_")
nr_pooled120_1_p <- data_replicaten_120_1$biol_pooled

pmv_test <- adonis2(dist_soorten_120_1 ~   biol_repl120_1_p + nr_pooled120_1_p, data=data_replicaten_120_1, permutations=9999) #,strata=biol_repl120_1_p:nr_pooled120_1_p)
pmv_test #sign voor biol_repl, niet voor nr_pooled

#station 840_1
biol_repl840_1_p <- data_replicaten_840_1$Biol_replicate
data_replicaten_840_1$biol_pooled <- paste(data_replicaten_840_1$Biol_replicate, data_replicaten_840_1$Nr_pooled, sep="_")
nr_pooled840_1_p <- data_replicaten_840_1$biol_pooled

pmv_test <- adonis2(dist_soorten_840_1 ~   biol_repl840_1_p + nr_pooled840_1_p, data=data_replicaten_840_1, permutations=9999) #,strata=biol_repl840_1_p:nr_pooled840_1_p)
pmv_test #sign voor biol_repl, niet voor nr_pooled

#station ZVL_1
biol_replZVL_1_p <- data_replicaten_ZVL_1$Biol_replicate
data_replicaten_ZVL_1$biol_pooled <- paste(data_replicaten_ZVL_1$Biol_replicate, data_replicaten_ZVL_1$Nr_pooled, sep="_")
nr_pooledZVL_1_p <- data_replicaten_ZVL_1$biol_pooled

pmv_test <- adonis2(dist_soorten_ZVL_1 ~   biol_replZVL_1_p + nr_pooledZVL_1_p, data=data_replicaten_ZVL_1, permutations=9999) #,strata=biol_replZVL_1_p:nr_pooledZVL_1_p)
pmv_test #sign voor biol_repl, niet voor nr_pooled


#factor biological replicates
repl_betadisper_120 <- betadisper(dist_soorten_120_1, biol_repl120_1_p, type="centroid")
repl_permdisp_120 <- permutest(repl_betadisper_120, permutations=9999)
repl_permdisp_120 # sign
plot(repl_betadisper_120, hull=FALSE, ellipse=TRUE)

repl_betadisper_840 <- betadisper(dist_soorten_840_1, biol_repl840_1_p, type="centroid")
repl_permdisp_840 <- permutest(repl_betadisper_840, permutations=9999)
repl_permdisp_840 # not sign => difference in location
plot(repl_betadisper_840, hull=FALSE, ellipse=TRUE)

repl_betadisper_ZVL <- betadisper(dist_soorten_ZVL_1, biol_replZVL_1_p, type="centroid")
repl_permdisp_ZVL <- permutest(repl_betadisper_ZVL, permutations=9999)
repl_permdisp_ZVL # not sign => differnce in community compostiion
plot(repl_betadisper_ZVL, hull=FALSE, ellipse=TRUE)

pairwise.adonis(dist_soorten, Station_p, p.adjust.m="BH", reduce=NULL, perm=9999)
pairwise.adonis(dist_soorten, Station_biol_repl_p, p.adjust.m="BH", reduce=NULL, perm=9999) #geen meerwaarde



```

A significant Permanova means one of three things. 1) There is a difference in the location of the samples (i.e. the average community composition), 2) There is a difference in the dispersion of the samples (i.e. the variability in the community composition), or 3) There is a difference in both the location and the dispersion.
So, if you get a significant Permanova you'll want to distinguish between the three options. That of course is why you need to run the permdisp. If you get a non-significant Permdisp you can conclude the first option above is the correct one. If you get a significant Permdisp then it is either the second or third option (that is there is definitely a difference in dispersion and maybe a difference in location). There is no fool-proof way to distinguish between these two, but looking at an MDS plot of the data will hopefully help you do so (also see note 2 below).

```{R permanova}
#pmv <- adonis(data_soorten^0.25 ~ Station * Station_biol_replicates, data=data_replicaten, permutations=999, method="jaccard")

#station 120
station120_p <- data_replicaten_120$Station
nr_pooled120_p <- data_replicaten_120$Nr_pooled

pmv_test <- adonis(dist_soorten_120 ~   station120_p * nr_pooled120_p, data=data_replicaten_120, permutations=9999) #,strata=biol_repl120_p:nr_pooled120_p)
pmv_test #sign voor biol_repl, niet voor nr_pooled

#station 840
biol_repl840_p <- data_replicaten_840$Biol_replicate
data_replicaten_840$biol_pooled <- paste(data_replicaten_840$Biol_replicate, data_replicaten_840$Nr_pooled, sep="_")
nr_pooled840_p <- data_replicaten_840$biol_pooled

pmv_test <- adonis2(dist_soorten_840 ~   biol_repl840_p + nr_pooled840_p, data=data_replicaten_840, permutations=9999) #,strata=biol_repl840_p:nr_pooled840_p)
pmv_test #sign voor biol_repl, niet voor nr_pooled

#station ZVL
biol_replZVL_p <- data_replicaten_ZVL$Biol_replicate
data_replicaten_ZVL$biol_pooled <- paste(data_replicaten_ZVL$Biol_replicate, data_replicaten_ZVL$Nr_pooled, sep="_")
nr_pooledZVL_p <- data_replicaten_ZVL$biol_pooled

pmv_test <- adonis2(dist_soorten_ZVL ~   biol_replZVL_p + nr_pooledZVL_p, data=data_replicaten_ZVL, permutations=9999) #,strata=biol_replZVL_p:nr_pooledZVL_p)
pmv_test #sign voor biol_repl, niet voor nr_pooled

#factor biological replicates
repl_betadisper_120 <- betadisper(dist_soorten_120, biol_repl120_p, type="centroid")
repl_permdisp_120 <- permutest(repl_betadisper_120, permutations=9999)
repl_permdisp_120 # not sign
plot(repl_betadisper_120, hull=FALSE, ellipse=TRUE)

repl_betadisper_840 <- betadisper(dist_soorten_840, biol_repl840_p, type="centroid")
repl_permdisp_840 <- permutest(repl_betadisper_840, permutations=9999)
repl_permdisp_840 # not sign => difference in location
plot(repl_betadisper_840, hull=FALSE, ellipse=TRUE)

repl_betadisper_ZVL <- betadisper(dist_soorten_ZVL, biol_replZVL_p, type="centroid")
repl_permdisp_ZVL <- permutest(repl_betadisper_ZVL, permutations=9999)
repl_permdisp_ZVL # not sign => differnce in community compostiion
plot(repl_betadisper_ZVL, hull=FALSE, ellipse=TRUE)

```


A significant Permanova means one of three things. 1) There is a difference in the location of the samples (i.e. the average community composition), 2) There is a difference in the dispersion of the samples (i.e. the variability in the community composition), or 3) There is a difference in both the location and the dispersion.
So, if you get a significant Permanova you'll want to distinguish between the three options. That of course is why you need to run the permdisp. If you get a non-significant Permdisp you can conclude the first option above is the correct one. If you get a significant Permdisp then it is either the second or third option (that is there is definitely a difference in dispersion and maybe a difference in location). There is no fool-proof way to distinguish between these two, but looking at an MDS plot of the data will hopefully help you do so (also see note 2 below).

