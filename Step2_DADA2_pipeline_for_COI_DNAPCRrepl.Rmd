######################################################################################
                                 DADA2 pipeline Paper
   "Towards harmonization of DNA metabarcoding for monitoring marine macrobenthos: 
 the effect of technical replicates and pooled DNA extractions on species detection"
                                Laure Van den Bulcke 
                                    januari 2021
#######################################################################################




##Introduction and package load
DADA2 will infer exact sequence variants from the data. Quality scores and abundances of all sequences are estimated per run, and an error model for each run is created. This error model makes it possible to correct erroneous bases, which reduces the total amount of sequences identified, hence making it possible to work with exact sequence variants (instead of having to cluster sequences based on a %identity threshold). For more information, see https://benjjneb.github.io/dada2/index.html

To use DADA2, we will first load some libraries. 

```{r package_load, echo=FALSE}
#LOAD PACKAGES
rm(list=ls())
library(dada2)
library(phyloseq)
library(ggplot2)
library(vegan)
```

##Preprocessing and quality check
Before we can go further with the DADA2 procedure, we first need to preprocess our samples. Adapters are in most cases already removed by the sequencing provider. However, primers are still present. We can remove these by using Trimmomatic. For primerset A: set a headcrop 26 and a tailcrop 26 (the length of the F and R primerset respectively)

After Primerremoval of the samples, we will check the quality of the samples. 
First we read in the names of the fastq files, and perform some string manipulation to get lists of the forward and reverse fastq files in matched order:

```{r preprocessing, echo=F}

#Set the path. Preferably, this is the same as your working directory
path <- "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/02_Trimmomatic/DNA_PCRrepl"

#Choose the working directory you want to work in
setwd(path)
getwd() #hele chunk laten lopen, anders geeft hij de working direction van ervoor (waarin het script staat?)

#make variables with sample names and forward and reverse reads (in the same order)
#Sort ensures forward/reverse reads are in same order
fnFs <- sort(list.files(path, pattern="1.fastq.gz", full.names = TRUE))
fnRs <- sort(list.files(path, pattern="2.fastq.gz", full.names = TRUE))

#Extract sample names, assuming filenames have format: SAMPLENAME_XXX.fastq  
# => moet een unieke naam zijn en 1e deel van splitsting is overal hetzelfde, 4e deel wél uniek
sample.names <- sapply(strsplit(basename(fnFs), "-"), `[`, 4)
```

Then, we can check the quality of our samples by using `rplotQualityProfile`. Here we will check the quality of the first two samples, both for the forward reads (fnFs) and for the reverse reads (fnRs). In addition, you can check the quality of the samples making use of **FastQC**.

```{r qualityprofiles}
plotQualityProfile(fnFs[1:2])
plotQualityProfile(fnRs[1:2])
plotQualityProfile(fnFs[31:32])
plotQualityProfile(fnRs[31:32])
```

As expected, the forward reads look quite good. The reverse reads however, are lowering in quality from (aanpassen) 200 bp onwards. Preferably, we will need to trim these sequences to remove low-quality sequence parts. 

This isn’t too worrisome, as DADA2 incorporates quality information into its error model which makes the algorithm robust to lower quality sequence, but trimming as the average qualities crash will improve the algorithm’s sensitivity to rare sequence variants. Based on these profiles, we will truncate the reverse reads at position 240 where the quality distribution crashes. 

>**Attention!** You should still be able to make the overlap of the sequences. For Primerset A, amplicons are 420 bp long. After trimming the primers, F reads are 300-26 = 274bp long, R reads are 300-60 = 240bp long. Primers are located at the start of the read, so they do not interfere with the overlap region. If the amplicon is 420bp and the read length 300bp, then the overlap region is 120 bp, even after primer removal. 
300 bp - primer (26) = 274 bp nog 10 afknippen
amplicon lentgh = 420. 

##Filtering and trimming

Assign the filenames 
```{r assign_filenames}
filt_path <- file.path(path, "filtered") # Place filtered files in filtered/ subdirectory
filtFs <- file.path(filt_path, paste0(sample.names, "_F_filt.fastq.gz")) #make name for forward filtered files
names(filtFs) <- sample.names #necessary for the dereplication and merging step
filtRs <- file.path(filt_path, paste0(sample.names, "_R_filt.fastq.gz")) #make name for reverse filtered files
names(filtRs) <- sample.names
```

For the filtering, we’ll use standard filtering parameters: maxN=0 (DADA2 requires no Ns), truncQ=2 and rm.phix=TRUE. maxEE will be set at 3, whereas standard this is set on 2. Here we will use a value of 3 to better compare with the Uparse pipeline (where a value of 3 is chosen as a standard). The maxEE parameter sets the maximum number of “expected errors” allowed in a read, which is a better filter than simply averaging quality scores.

Based on the quality profiles, we will remove 10 bp from the forward read  (300-26(primer) = 264). For the reverse read, quality drops below 30 after ca 210 bp. We will remove the part after 210 bp. **These values can however change depending of the dataset you are looking at**.

```{r filtering_trimming}
out <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, truncLen=c(274,210),   
              maxN=0, maxEE=c(3,3), truncQ=2, rm.phix=TRUE,
              compress=TRUE, multithread=TRUE) # On Windows set multithread=FALSE, R server = T
```

##Estimate the error rates
The DADA2 algorithm depends on a parametric error model (err) and every amplicon dataset has a different set of error rates. The learnErrors method learns the error model from the data, by alternating estimation of the error rates and inference of sample composition until they converge on a jointly consistent solution. As in many optimization problems, the algorithm must begin with an initial guess, for which the maximum possible error rates in this data are used (the error rates if only the most abundant sequence is correct and all the rest are errors).

1e8 => maar 3 samples gebruikt
nu 1e9 => 32 samples (duurt wel wat langer)

```{r error_estimation, echo = FALSE}
errF <- learnErrors(filtFs, nbases=1e9, multithread=TRUE)
errR <- learnErrors(filtRs, nbases=1e9, multithread=TRUE)
```

>**Note** Possible Warning message:In dada(drps, err = NULL, selfConsist = TRUE, multithread = multithread) :Self-consistency loop terminated before convergence. First thing you can do is increase the number of loops by the option MAX_CONSIST = 25. Another thing that is possible is to see if the number after 10 loops has become increasingly smaller by the option dada2:::checkConvergence(dadaRs[[1]]) => rates should get closer to convergence

It is always worthwhile, as a sanity check if nothing else, to visualize the estimated error rates. The black line (=estimated scores) should more or less follow the expected rates (=red line). The error rates for each possible transition (eg. A->C, A->G, …) are shown. Points are the observed error rates for each consensus quality score. The black line shows the estimated error rates after convergence. The red line shows the error rates expected under the nominal definition of the Q-value.

??? Niet mooi op zwarte lijn, maar indien ik nread=1e7 doe, duurt de berekening héél lang (en heb ik het opgegeven voordat ik resultaat had)

```{r error_plot, eval=FALSE}
plotErrors(errF, nominalQ=TRUE)
plotErrors(errR, nominalQ=TRUE) 
```

>**Note** Parameter learning is computationally intensive, so by default the *learnErrors* function uses only a subset of the data (the first 1M reads). If the plotted error model does not look like a good fit, try increasing the nreads parameter to see if the fit improves.


##Dereplication, Inferring sequence variants and merging each sample
Dereplication combines all identical sequencing reads into “unique sequences” with a corresponding “abundance”: the number of reads with that unique sequence. Dereplication substantially reduces computation time by eliminating redundant comparisons.

Dereplication in the DADA2 pipeline has one crucial addition from other pipelines: DADA2 retains a summary of the quality information associated with each unique sequence. The consensus quality profile of a unique sequence is the average of the positional qualities from the dereplicated reads. These quality profiles inform the error model of the subsequent denoising step, significantly increasing DADA2’s accuracy.

>**Note** Because we are working with large datasets, we will make use of the protocol specified for large datasets.

```{r dereplication}
mergers <- vector("list", length(sample.names))	#initate a list where the final data will be stored
names(mergers) <- sample.names
for(sam in sample.names) {			#for each sample, do the commands in this loop
  cat("Processing:", sam, "\n")
    derepF <- derepFastq(filtFs[[sam]])					#dereplication of forward reads
    ddF <- dada(derepF, err=errF, multithread=TRUE)		#inferring sequence variants of forward reads
    derepR <- derepFastq(filtRs[[sam]])					#dereplication of reverse reads
    ddR <- dada(derepR, err=errR, multithread=TRUE)		#inferring sequence variants of forward reads
    merger <- mergePairs(ddF, derepF, ddR, derepR)		#merge forward and reverse reads
    mergers[[sam]] <- merger							#save merged reads in the list
}


```

##Construct sequence table

We can now construct a sequence table of our samples, a higher-resolution version of the OTU table produced by traditional methods. This table can be saved as an RDS file. 

```{r sequence_table}
#make a sequence table of the merged reads with the counts per sample
path <- "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/02_Trimmomatic/DNA_PCRrepl"
setwd(path)
seqtab <- makeSequenceTable(mergers)		

#look into the dimensions of your table
dim(seqtab) #99 3908

#Inspect distribution of sequence length
table(nchar(getSequences(seqtab))) # most ASV(1290) are 313 bp long

#save the sequence table as an RDS file, change the name according to the run
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
saveRDS(seqtab, "seqtab_DNAPCRrepl.rds") 
```

By applying the table function, we could check the distribution of the sequence lengths. This gives an idea if there are larger or smaller sequences than expected. Sequences that are much longer or shorter than expected may be the result of non-specific priming, and may be worth removing (eg. `r seqtab2 <- seqtab[,nchar(colnames(seqtab)) %in% seq(250,256)]`). This is analogous to “cutting a band” in-silico to get amplicons of the targeted length.

##Remove chimeras
The core dada method removes substitution and indel errors, but chimeras remain. Fortunately, the accuracy of the sequences after denoising makes identifying chimeras simpler than it is when dealing with fuzzy OTUs: all sequences which can be exactly reconstructed as a bimera (two-parent chimera) from more abundant sequences.

```{r chimera_removal}
#R code in case of using a sequence table from a single run
seqtab.nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)
dim(seqtab.nochim) #99 3569

#Optional saving of the sequence table as an RDS file, change the name according to the run
#setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
#saveRDS(seqtab.nochim, "seqtab.nochim_DNAPCRrepl.rds") 
```
## add metadata and change sample names accordingly

```{r creating names file and optional rarefaction}
# Select only the code and the replicate number
names_file<-paste0(lapply(rownames(seqtab.nochim),substr,4,6))
te_lang <- names_file[1:9]
i=1
for (x in te_lang){
   x <- substr(x,0,2)
   x <- paste0("0",x)
   names_file[i] <- x
   i = i+1
}
names_file<-as.data.frame(names_file)

names_file<-cbind(names_file,group=c("120_B_1_1_D","120_C_1_1_D","120_A_1_1_D","ZVL_C_1_1_D","120_B_2_1_D","ZVL_A_1_1_D","120_C_2_1_D","120_B_3_1_D","840_C_1_1_D","840_B_1_1_D","120_A_2_1_D","120_A_3_1_D","ZVL_B_1_1_D","120_C_3_1_D","840_A_1_1_D","840_A_2_1_D","120_A_4_1_D","840_C_2_1_D","120_B_4_1_D","120_C_4_1_D","ZVL_C_2_1_D","120_B_5_1_D","120_A_5_1_D","120_C_5_1_D","ZVL_B_2_1_D","840_B_2_1_D","ZVL_A_2_1_D","120_B_6_1_D","120_C_6_1_D","120_A_6_1_D","840_A_3_1_D","840_A_4_1_D","840_A_5_1_D","840_A_6_1_D","840_B_3_1_D","840_B_4_1_D","840_B_5_1_D","840_B_6_1_D","840_C_3_1_D","840_C_4_1_D","840_C_5_1_D","840_C_6_1_D","ZVL_A_3_1_D","ZVL_A_4_1_D","ZVL_A_5_1_D","ZVL_A_6_1_D","ZVL_B_3_1_D","ZVL_B_4_1_D","ZVL_B_5_1_D","ZVL_B_6_1_D","ZVL_C_3_1_D","ZVL_C_4_1_D","ZVL_C_5_1_D","ZVL_C_6_1_D","120_B_13_2_D","120_B_46_2_D","120_B_24_2_D","120_B_5462_4_D","120_B_2135_4_D","120_B_6513_4_D","120_B_513246_6_D","120_B_624351_6_D","120_B_351624_6_D","840_C_26_2_D","840_C_15_2_D","840_C_42_2_D","840_C_4153_4_D","840_C_3642_4_D","840_C_6315_4_D","840_C_531426_6_D","840_C_642531_6_D","840_C_315264_6_D","ZVL_A_51_2_D","ZVL_A_46_2_D","ZVL_A_35_2_D","ZVL_A_3624_4_D","ZVL_A_2513_4_D","ZVL_A_1462_4_D","ZVL_A_624135_6_D","ZVL_A_246351_6_D","ZVL_A_135246_6_D",
"120_A_1_P","120_A_2_P","120_A_3_P", "120_B_1_P","120_B_2_P","120_B_3_P", "120_C_1_P","120_C_2_P","120_C_3_P", "ZVL_A_1_P","ZVL_A_2_P","ZVL_A_3_P","ZVL_B_1_P","ZVL_B_2_P","ZVL_B_3_P", "ZVL_C_1_P","ZVL_C_2_P","ZVL_C_3_P"))

#add the metadata to the sample name 
names_file$names_file<-paste(names_file$names_file,names_file$group,sep = '_')
# change the  rownames to the new shorter names
rownames(seqtab.nochim)<-names_file$names_file
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
saveRDS(seqtab.nochim, "seqtab.nochim_DNAPCRrepl.rds") 
```

##Track reads through the pipeline
As a final check of our progress, we will look to a number of reads that made it through each step of the pipeline; This gives an idea of how many sequences per read were retained after each step of the pipeline. 

```{r read_tracking, eval=FALSE}
getN <- function(x) sum(getUniques(x))
track <- cbind(out,sapply(mergers,getN), rowSums(seqtab.nochim))
# If processing a single sample, remove the sapply calls: e.g. replace sapply(dadaFs, getN) with getN(dadaFs)
colnames(track) <- c("input", "filtered","denoised","nonchim")
rownames(track) <- names_file$names_file
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
write.csv(track, file = "Readnumbers_DNAPCRrepl.csv")
```



>**Note** When checking this table, you should notice that the only step where you loose a major part of your reads, is the filtering step. For the other steps, only a minority of reads should be lost! If for example you loose a lot of reads in the merging step, you should adjust the `r truncLen` option. 

## Rarify the dataset to correct for unequal sequencing depth per sample
To rarify the data, we determine the number of reads obtained for each sample, and then sort the samples so that the sample with lowest read numbers is the first sample.

S27_ZVL_A_2_1_D
215
0S6_ZVL_A_1_1_D
234
S90_120_C_3_P
5066
S29_120_C_6_1_D
7061
S89_120_C_2_P
12799
S37_840_B_5_1_D
24661
S88_120_C_1_P
28060
S62_120_B_624351_6_D
40135
S78_ZVL_A_1462_4_D
49799
S93_ZVL_A_3_P
63112
S70_840_C_531426_6_D
71257


-----------------------------------------------------------------
We will rarify DNA samples on 24000 reads
               PCR samples on 12000 reads
So first, subsets are made.
-----------------------------------------------------------------

```{r rarefaction}
# count number of sequences per sample and sort from small to large
NumberOfSequences <-as.data.frame(sort.default(rowSums(seqtab.nochim[1:nrow(seqtab.nochim),]))) 

subset_rarify_DNA <- seqtab.nochim[1:81,]
seqtabDNA.nochim <- subset_rarify_DNA

#create rarefaction curves
pdf("RarefactionCurveDNArepl.pdf")
rarecurve(subset_rarify_DNA,step = 1000, sample = 24000)
dev.off()

# remove samples with low sequencing depth   
remove_samples_DNA<-c("0S6_ZVL_A_1_1_D","S27_ZVL_A_2_1_D","S29_120_C_6_1_D")
seqtabDNA.nochim2<-seqtab.nochim[!rownames(seqtab.nochim) %in% remove_samples_DNA,]
names_file_DNA <- names_file[1:81,]
names_file_DNA2<-names_file_DNA[!names_file_DNA$names_file_DNA %in% remove_samples_DNA,]

setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
saveRDS(seqtabDNA.nochim2, "seqtabDNA.nochim2.rds") 

raremax<-24000
seqtabDNA.nochim2.rarified<-rrarefy(seqtabDNA.nochim2[1:nrow(seqtabDNA.nochim2),],raremax)
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
saveRDS(seqtabDNA.nochim2.rarified, "seqtabDNA.nochim2.rarified.rds") 
```
#PCR replciates

S90_120_C_3_P
5066

S89_120_C_2_P
12799

S88_120_C_1_P
28060


```{r rarefaction pcr repl}

#make subset pcr repl
subset_rarify_PCR <- seqtab.nochim[82:99,]
seqtabPCR.nochim <- subset_rarify_PCR

# count number of sequences per sample and sort from small to large
NumberOfSequences_PCR <-as.data.frame(sort.default(rowSums(subset_rarify_PCR[1:nrow(subset_rarify_PCR),]))) 

#create rarefaction curves
pdf("RarefactionCurvePCRrepl.pdf")
rarecurve(subset_rarify_PCR,step = 1000, sample = 12000)
dev.off()


# remove samples with low sequencing depth   
remove_samples_PCR <-c("S90_120_C_3_P")
seqtabPCR.nochim2<-subset_rarify_PCR[!rownames(subset_rarify_PCR) %in% remove_samples_PCR,]
names_file_PCR <- names_file[82:99,]
names_file_PCR2<-names_file_PCR[!names_file_PCR$names_file_PCR %in% remove_samples_PCR,]
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
saveRDS(seqtabPCR.nochim2, "seqtabPCR.nochim2.rds") 
raremax<-12000
seqtabPCR.nochim2.rarified <-rrarefy(seqtabPCR.nochim2[1:nrow(seqtabPCR.nochim2),],raremax)
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
saveRDS(seqtab.nochim2.rarified, "seqtabPCR.nochim2.rarified.rds") 


#same number of ASV?
table_PCR_test <- t(seqtabPCR.nochim2.rarified)
NumberOfASV_PCR <- colSums(table_PCR_test != 0)
barplot(NumberOfASV_PCR,beside = T, ylim = c(0,200), ylab = "# ASVs", las =2, main = "Number of ASVs PCr test", cex.axis= 0.75)

gem_120_A <- mean(NumberOfASV_PCR[1:3])
sd_120_A <- sd(NumberOfASV_PCR[1:3])
gem_120_B <- mean(NumberOfASV_PCR[4:6])
sd_120_B <- sd(NumberOfASV_PCR[4:6])
gem_120_C <- mean(NumberOfASV_PCR[7:8]) #1 staal verwijderd
sd_120_C <- sd(NumberOfASV_PCR[7:8])

gem_ZVL_A <- mean(NumberOfASV_PCR[9:11])
sd_ZVL_A <- sd(NumberOfASV_PCR[9:11])
gem_ZVL_B <- mean(NumberOfASV_PCR[12:14])
sd_ZVL_B <- sd(NumberOfASV_PCR[12:14])
gem_ZVL_C <- mean(NumberOfASV_PCR[15:17])
sd_ZVL_C <- sd(NumberOfASV_PCR[15:17])

overzicht_ASV <- data.frame("Sample" =c("120_A", "120_B", "120_C","ZVL_A", "ZVL_B", "ZVL_C"), "Station"= c(rep("120", 3),rep("ZVL", 3)), "Biol_repl"=(rep(c("A", "B", "C"),2)), "Mean" = c(gem_120_A,gem_120_B,gem_120_C, gem_ZVL_A,gem_ZVL_B,gem_ZVL_C), "sd"=c(sd_120_A,sd_120_B, sd_120_C, sd_ZVL_A,sd_ZVL_B, sd_ZVL_C))
library(ggplot2)
ggplot(data = overzicht_ASV, aes(Biol_repl, y=Mean,fill=Biol_repl)) +
  geom_col() +scale_fill_brewer(palette="Greys")+
  ggtitle("DNA replicates") +
  labs(x="", y="Mean number of ASV") + facet_grid(.~Station) + 
  theme_bw()+  geom_errorbar(aes(ymin=Mean-sd, ymax=Mean+sd), width=.2, position=position_dodge(.9)) +theme(axis.title.x=element_blank(),axis.ticks.x=element_blank(),plot.title = element_text(hjust=0.5), legend.position = "none")


```

##Taxonomy assignment

In the last step, we are going to add taxonomy to the different sequence variants.  

```{r taxonomy_assignment}
#assignTaxonomy: RDP, change standard settings to minboot=80

#TOGETHER
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
tax_all <- assignTaxonomy(seqtab.nochim, "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/Morphology_database.fasta", multithread = TRUE, minBoot=80)


#DNA repl
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
taxDNA <- assignTaxonomy(seqtabPCR.nochim, "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/Morphology_database.fasta", multithread = TRUE, minBoot=80)
saveRDS(tax, "tax_DNA.rds")

taxDNA_rarified <- assignTaxonomy(seqtabDNA.nochim2.rarified, "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/Morphology_database.fasta", multithread = TRUE, minBoot=80)
saveRDS(taxDNA_rarified, "tax_rarified_DNA.rds")


#PCR repl
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
taxPCR <- assignTaxonomy(seqtabDNA.nochim, "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/Morphology_database.fasta", multithread = TRUE, minBoot=80)
saveRDS(tax, "tax_PCR.rds")

taxPCR_rarified <- assignTaxonomy(seqtabPCR.nochim2.rarified, "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/Morphology_database.fasta", multithread = TRUE, minBoot=80)
saveRDS(taxPCR_rarified, "tax_rarified_PCR.rds")

```



##Write taxonomy table to output txt file

We will write two separate file formats. First, we can make use of a count table (not normalized!). Second, we will make a frequence table, which takes into account different sequencing depths. 

COUNTTABLE BASED ON RARIFIED DATA AND NON RARIFIED

```{r count_table, echo=F}
#all
table_all <- as.data.frame(t(seqtab.nochim))
rownames(table_all) <- NULL
taxa_all <- as.data.frame(tax_all)
table_all$Kingdom<-taxa_all$Kingdom
table_all$Phylum<-taxa_all$Phylum
table_all$Class<-taxa_all$Class
table_all$Order<-taxa_all$Order
table_all$Family<-taxa_all$Family
table_all$Genus<-taxa_all$Genus
table_all$Species<-taxa_all$Species
table_all$ASV<-rownames(tax_all)



#For DNA repl and  PCR repl separate (other rarifraction max)

tableDNA<-as.data.frame(t(seqtabDNA.nochim))
tableDNA_rarified <- as.data.frame(t(seqtabDNA.nochim2.rarified))

tablePCR <-as.data.frame(t(seqtabPCR.nochim))
tablePCR_rarified <- as.data.frame(t(seqtabPCR.nochim2.rarified))

#remove rownames
rownames(tableDNA)<-NULL
rownames(tableDNA_rarified)<-NULL
rownames(tablePCR)<-NULL
rownames(tablePCR_rarified)<-NULL

#add extra columns to table to add taxonomy, for this, first transform the tax object to a data frame
taxaDNA<-as.data.frame(taxDNA)
tableDNA$Kingdom<-taxaDNA$Kingdom
tableDNA$Phylum<-taxaDNA$Phylum
tableDNA$Class<-taxaDNA$Class
tableDNA$Order<-taxaDNA$Order
tableDNA$Family<-taxaDNA$Family
tableDNA$Genus<-taxaDNA$Genus
tableDNA$Species<-taxaDNA$Species
tableDNA$ASV<-rownames(taxDNA)

taxaDNA_rarified<-as.data.frame(taxDNA_rarified)
tableDNA_rarified$Kingdom<-taxaDNA_rarified$Kingdom
tableDNA_rarified$Phylum<-taxaDNA_rarified$Phylum
tableDNA_rarified$Class<-taxaDNA_rarified$Class
tableDNA_rarified$Order<-taxaDNA_rarified$Order
tableDNA_rarified$Family<-taxaDNA_rarified$Family
tableDNA_rarified$Genus<-taxaDNA_rarified$Genus
tableDNA_rarified$Species<-taxaDNA_rarified$Species
tableDNA_rarified$ASV<-rownames(taxDNA_rarified)



taxaPCR<-as.data.frame(taxPCR)
tablePCR$Kingdom<-taxaPCR$Kingdom
tablePCR$Phylum<-taxaPCR$Phylum
tablePCR$Class<-taxaPCR$Class
tablePCR$Order<-taxaPCR$Order
tablePCR$Family<-taxaPCR$Family
tablePCR$Genus<-taxaPCR$Genus
tablePCR$Species<-taxaPCR$Species
tablePCR$ASV<-rownames(taxPCR)

taxaPCR_rarified<-as.data.frame(taxPCR_rarified)
tablePCR_rarified$Kingdom<-taxaPCR_rarified$Kingdom
tablePCR_rarified$Phylum<-taxaPCR_rarified$Phylum
tablePCR_rarified$Class<-taxaPCR_rarified$Class
tablePCR_rarified$Order<-taxaPCR_rarified$Order
tablePCR_rarified$Family<-taxaPCR_rarified$Family
tablePCR_rarified$Genus<-taxaPCR_rarified$Genus
tablePCR_rarified$Species<-taxaPCR_rarified$Species
tablePCR_rarified$ASV<-rownames(taxPCR_rarified)


setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
#write table to output file
write.table(tableDNA,"CountTable_nonrarified_DNA.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)	
write.table(tableDNA_rarified,"CountTable_rarified_DNA.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)	
write.table(tablePCR,"CountTable_nonrarified_PCR.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)	
write.table(tablePCR_rarified,"CountTable_rarified_PCR.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)	

write.table(table_all,"CountTable_nonrarified_all.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)	


#get a list of the different families detected
#Families <- as.data.frame(unique(taxaDNA$Family))
#write.table(Families,"Families_DNAPCRrepl.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)
```

#export ASV sequences to fasta file
#library(seqRFLP)
#setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
#dataframe2fas(rownames(table),file = "seqtab_nochim2_ASV_DNAPCRrepl.fasta")

make frequency table: based on nonrarified data (so can also be on whole dataset)
FREQUENCY TABLE BASED ON NON RARIFIED DATA AND ON 

```{r frequency_table from non-rarified table}


#calculate number of samples, store sample names in list
samplenumber_all <- ncol(table_all)-8
samples_all<-colnames(table_all)[1:samplenumber_all]


samplenumber_PCR<-ncol(tablePCR)-8
samples_PCR<-colnames(tablePCR)[1:samplenumber_PCR]

samplenumber_DNA<-ncol(tableDNA)-8
samples_DNA<-colnames(tableDNA)[1:samplenumber_DNA]

#add new columns with percentages
freq_table_all <- data.frame(matrix(nrow = nrow(table_all), ncol=samplenumber_all, dimnames = list(c(rownames(table_all)), c(colnames(table_all[1:samplenumber_all])))))
freq_table_DNA <- data.frame(matrix(nrow = nrow(tableDNA), ncol=samplenumber_DNA, dimnames = list(c(rownames(tableDNA)), c(colnames(tableDNA[1:samplenumber_DNA])))))
freq_table_PCR <- data.frame(matrix(nrow = nrow(tablePCR), ncol=samplenumber_PCR, dimnames = list(c(rownames(tablePCR)), c(colnames(tablePCR[1:samplenumber_PCR])))))

#calculate frequencies
for (i in 1:samplenumber_all){
  freq_table_all[,i] <- table_all[,i]/colSums(table_all[1:samplenumber_all])[i]
}

#add species information to the frequency table
freq_table_all$Kingdom<-taxa_all$Kingdom
freq_table_all$Phylum<-taxa_all$Phylum
freq_table_all$Class<-taxa_all$Class
freq_table_all$Order<-taxa_all$Order
freq_table_all$Family<-taxa_all$Family
freq_table_all$Genus<-taxa_all$Genus
freq_table_all$Species <- taxa_all$Species 



for (i in 1:samplenumber_DNA){
  freq_table_DNA[,i] <- tableDNA[,i]/colSums(tableDNA[1:samplenumber_DNA])[i]
}

#add species information to the frequency table
freq_table_DNA$Kingdom<-taxaDNA$Kingdom
freq_table_DNA$Phylum<-taxaDNA$Phylum
freq_table_DNA$Class<-taxaDNA$Class
freq_table_DNA$Order<-taxaDNA$Order
freq_table_DNA$Family<-taxaDNA$Family
freq_table_DNA$Genus<-taxaDNA$Genus
freq_table_DNA$Species <- taxaDNA$Species 

for (i in 1:samplenumber_PCR){
  freq_table_PCR[,i] <- tablePCR[,i]/colSums(tablePCR[1:samplenumber_PCR])[i]
}

#add species information to the frequency table
freq_table_PCR$Kingdom<-taxaPCR$Kingdom
freq_table_PCR$Phylum<-taxaPCR$Phylum
freq_table_PCR$Class<-taxaPCR$Class
freq_table_PCR$Order<-taxaPCR$Order
freq_table_PCR$Family<-taxaPCR$Family
freq_table_PCR$Genus<-taxaPCR$Genus
freq_table_PCR$Species <- taxaPCR$Species 


setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
#write to new output table
write.table(freq_table_DNA,"FrequencyTable_DNA.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)
write.table(freq_table_PCR,"FrequencyTable_PCR.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)
write.table(freq_table_all, "FrequencyTable_all.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)
# This pipeline is followed by the adonis.R script
# Do not delete your workspace (it is still required for the Step4_statistics_and_graphs.R script)

```





```{R clean-up script}

rm(ddF_pool,ddF_pseudopool,ddR_pool, ddR_pseudopool,derepF_pool,derepF_pseudopool,derepR_pool,derepR_pseudopool,Families_pool,freq_table,merger_pool,merger_pseudopool,mergers_pool,names_file, names_file2,seqtab_pool,seqtab_pool.nochim, seqtab.nochim,seqtab.nochim2, seqtab.nochim2.rarified, table, table_pool, tax, tax_pool, taxa_pool, track_pool)


```






















------------------------------------
TEST FOR EFFECT OF DADA(POOL=TRUE)
------------------------------------

```{R Test R dada(pool=TRUE)}


mergers_pool <- vector("list", length(sample.names))	#initate a list where the final data will be stored
names(mergers_pool) <- sample.names
for(sam in sample.names) {			#for each sample, do the commands in this loop
  cat("Processing:", sam, "\n")
    derepF_pool <- derepFastq(filtFs[[sam]])					#dereplication of forward reads
    ddF_pool <- dada(derepF_pool, err=errF, multithread=TRUE,pool=TRUE)		#inferring sequence variants of forward reads
    derepR_pool <- derepFastq(filtRs[[sam]])					#dereplication of reverse reads
    ddR_pool <- dada(derepR_pool, err=errR, multithread=TRUE,pool=TRUE)		#inferring sequence variants of reverse reads
    merger_pool <- mergePairs(ddF_pool, derepF_pool, ddR_pool, derepR_pool)		#merge forward and reverse reads
    mergers_pool[[sam]] <- merger_pool							#save merged reads in the list
}


```

```{R}
#make a sequence table of the merged reads with the counts per sample
seqtab_pool <- makeSequenceTable(mergers_pool)		

#remove chimera's
seqtab_pool.nochim <- removeBimeraDenovo(seqtab_pool, method="consensus", multithread=TRUE, verbose=TRUE)
rownames(seqtab_pool.nochim)<-names_file$names_file
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
saveRDS(seqtab_pool.nochim, "seqtab_pool.nochim_DNAPCRrepl.rds") 

#track reads
track_pool <- cbind(out,sapply(mergers_pool,getN), rowSums(seqtab_pool.nochim))
colnames(track_pool) <- c("input", "filtered","denoised","nonchim")
rownames(track_pool) <- names_file$names_file
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
write.csv(track_pool, file = "Readnumbers_pool_DNAPCRrepl.csv")

#Assign taxonomy
setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
tax_pool <- assignTaxonomy(seqtab_pool.nochim, "/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/Morphology_database.fasta", multithread = TRUE, minBoot=80)
saveRDS(tax_pool, "tax_pool_DNAPCRrepl.rds")

#count table
table_pool<-as.data.frame(t(seqtab_pool.nochim))
#export ASV sequences to fasta file
library(seqRFLP)
dataframe2fas(rownames(table_pool),file = "seqtab_pool_nochim2_ASV_DNAPCRrepl.fasta")
rownames(table_pool)<-NULL
#add extra columns to table to add taxonomy, for this, first transform the tax object to a data frame
taxa_pool<-as.data.frame(tax_pool)
table_pool$Kingdom<-taxa_pool$Kingdom
table_pool$Phylum<-taxa_pool$Phylum
table_pool$Class<-taxa_pool$Class
table_pool$Order<-taxa_pool$Order
table_pool$Family<-taxa_pool$Family
table_pool$Genus<-taxa_pool$Genus
table_pool$Species<-taxa_pool$Species
table_pool$ASV<-rownames(taxa_pool)

setwd("/home/genomics/lvandenbulcke/2019_COI_Thorntonbank/04_R_output/DNA_PCRrepl")
#write table to output file
write.table(table_pool,"CountTable_pool_DNAPCRrepl.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)	

#get a list of the different families detected
Families_pool <- as.data.frame(unique(taxa_pool$Family))
write.table(Families,"Families_DNAPCRrepl.txt",sep=",",col.names=TRUE,row.names=FALSE,quote=FALSE)

```






